// THERE IS A BUG! Draft messages are immutable, so when you modify an
// existing one, it is deleted and a new one with a new ID and
// the modifications is created in its place. At least I think.
// Consider using the DraftMessage type instead of Message when
// interacting with drafts?



// .GS supports most of js 1.8.5
// 'let' is not supported
// all times are in the spreadsheets timezone except when using epoch time in search queries

var constants = {
  // row and column indexes are 0 based, 2 would be "C"
  // sheet.getRange() and other functions take 1 based indexes though, smh
  // These constants are 0 based, so add 1 when calling a google function
  col: {
    msgId: 0,
    msgTo: 1,
    msgSubject: 2,
    status: 3,
    sendDate: 4,
    //repeat: 5,
    keepDraft: 5
  },
  statusToColor: {
    "Scheduled": "#93c47d",
    "Not Scheduled": "#ffd966",
    "Delivered": "#6d9eeb",
    "Date is in the past": "#e06666",
	"Not a draft": "#e06666"
  },
  firstDataRow: 1,
  // the first row that is data, not a header
  
  scheduleSheetName: "Gmail Schedule",
  // the name of the main sheet
  
  // keys for sheet key-value storage
  propertyKeys: {
	// the most recent imported draft in UNIX epoch
	gmailNewestDraftCreationDate: "gmailNewestDraftCreationDate"
  }
};

function initialize() {

    var sheet = SpreadsheetApp.getActive().getSheetByName(constants.scheduleSheetName);
	var documentProperties = PropertiesService.getDocumentProperties();
	
    var ids = sheet.getRange(constants.firstDataRow + 1, constants.col.msgId + 1, sheet.getLastRow(), 1).getValues();
	var statuses = sheet.getRange(constants.firstDataRow + 1, constants.col.status + 1, sheet.getLastRow(), 1).getValues();
	
	// check existance and update content for unsent drafts
	for (var i = 0; i < statuses.length; i++) {
		var status = statuses[i][0];
		switch (status) { // look at this unmaintainable switch ;)
			case "Delivered":
			case "Date is in the past":
			case "Not a draft":
			case "":
			    continue;				
		}
		
		var id = ids[i][0];
		var message = tryGetGmailMessageById(id);
		
		if (message == null || !message.isDraft()) {
			var statusCell = sheet.getRange(constants.firstDataRow + i + 1, constants.col.status + 1);
			statusCell.setValue("Not a draft");
			statusCell.setBackground(constants.statusToColor["Not a draft"]);
			continue;
		}
		
		var toCell = sheet.getRange(constants.firstDataRow + i + 1, constants.col.msgTo + 1);
		toCell.setValue(message.getTo());
		
		var subjectCell = sheet.getRange(constants.firstDataRow + i + 1, constants.col.msgSubject + 1);
		subjectCell.setValue(message.getSubject());
	}
		
	// import any new drafts
	// !!!!!!! Note that searching for messages before or after UNIX epoch time is an UNDOCUMENTED feature !!!!!!!
	// the most granularity for searching using a documented query is by day, eg "after:2018/04/13"
	// search queries behave as if they are entered into the gmail web app search box
	
	var newestDraftDate = parseInt(documentProperties.getProperty(constants.propertyKeys.gmailNewestDraftCreationDate) || "1");
	
	var searchQuery = "in:draft after:" + newestDraftDate;
	var searchStart = 0;
	var batchSize = 50;
	var firstEmptyRow = sheet.getLastRow() + 1;
	do {
		var threads = GmailApp.search(searchQuery, searchStart, batchSize)
		searchStart += batchSize;
		
		// collect any drafts from each thread
        var drafts = [];
        for(var i = 0; i < threads.length; i++) {
            var threadMessages = threads[i].getMessages();
            for(var j = 0; j < threadMessages.length; j++) {
                var threadMessage = threadMessages[j];
				if (!threadMessage.isDraft()) {
					continue;
				}
				
				drafts.push(threadMessage);
            }
        }
        
        
		for(var i = 0; i < drafts.length; i++) {
			var draft = drafts[i];
			var date = (draft.getDate().getTime() / 1000) + 1;
			// for drafts the date is the creation date, for delivered mail it's the send date
			// + 1 to prevent the newest draft getting imported multiple times
			// because the query "after:n" really means "at_or_after:n"
			newestDraftDate = Math.max(newestDraftDate, date);
			
			var idCell = sheet.getRange(firstEmptyRow + 1, constants.col.msgId + 1);
			idCell.setValue(draft.getId());
			
			var toCell = sheet.getRange(firstEmptyRow + 1, constants.col.msgTo + 1);
			toCell.setValue(draft.getTo());
			
			var subjectCell = sheet.getRange(firstEmptyRow + 1, constants.col.msgSubject + 1);
			subjectCell.setValue(draft.getSubject());
			
			firstEmptyRow++;
		}
	} while(drafts.length === batchSize);
	
	documentProperties.setProperty(constants.propertyKeys.gmailNewestDraftCreationDate, newestDraftDate.toString());
  
    updateSendTriggers();
}

function updateSendTriggers() {
  
    // delete existing triggers first
    var triggers = ScriptApp.getProjectTriggers();
    for (var i = 0; i < triggers.length; i++) {
        if (triggers[i].getHandlerFunction() === "sendMail") {
            ScriptApp.deleteTrigger(triggers[i]);
        }
    }
  
  
    var sheet = SpreadsheetApp.getActive().getSheetByName(constants.scheduleSheetName);
    var data = sheet.getDataRange().getValues();
    var time = new Date();
    for (var rowIndex = constants.firstDataRow; rowIndex < data.length; rowIndex++) {
        
        var row = data[rowIndex];
        var sendDateStr = row[constants.col.sendDate];
		
        var statusCell = sheet.getRange(rowIndex + 1, constants.col.status + 1)
        var status = statusCell.getValue();
        
        if (status === "Delivered") {
            continue;
        }
		
        
		var id = sheet.getRange(rowIndex + 1, constants.col.msgId + 1).getValue()
		var message = tryGetGmailMessageById(id)
		if (message == null) {
            statusCell.setValue("Not a draft")
            statusCell.setBackground(constants.statusToColor["Not a draft"] || "white")
			continue;
		}
        
        if (sendDateStr === "") {
            statusCell.setValue("Not Scheduled")
            statusCell.setBackground(constants.statusToColor["Not Scheduled"] || "white")
            continue;
        }
      
        var sendDate = new Date(sendDateStr);
      
        if (sendDate <= time) {
            statusCell.setValue("Date is in the past")
            statusCell.setBackground(constants.statusToColor["Date is in the past"] || "white")
            continue;
        }
		
        
        ScriptApp.newTrigger("sendMail")
            .timeBased()
            .at(sendDate)
            .inTimezone(SpreadsheetApp.getActiveSpreadsheet().getSpreadsheetTimeZone())
            .create();
        
        statusCell.setValue("Scheduled")
        statusCell.setBackground(constants.statusToColor["Scheduled"] || "white")
        
    }
  
}

// this trigger is called each time a message should be sent and iterates through all messages
// so sending all messages is O(n * m)
function sendMail() {
    var sheet = SpreadsheetApp.getActive().getSheetByName(constants.scheduleSheetName);
    var data = sheet.getDataRange().getValues();
    var time = new Date().getTime();
    for (var row = constants.firstDataRow; row < data.length; row++) {
        if (data[row][constants.col.status] !== "Scheduled") {
            continue;
        }
        
        var schedule = data[row][constants.col.sendDate];
        if ((schedule === "") || (time > schedule.getTime())) {
            continue;
        }
      
        var message = GmailApp.getMessageById(data[row][constants.col.msgId]);
        var body = message.getBody();
        var options = {
            cc: message.getCc(),
            bcc: message.getBcc(),
            htmlBody: body,
            replyTo: message.getReplyTo(),
            attachments: message.getAttachments()
        }

        GmailApp.sendEmail(message.getTo(), message.getSubject(), body, options);
              
        var isKeepingDraft = data[row][constants.col.keepDraft].toUpperCase();
        
        if (isKeepingDraft === "" || isKeepingDraft === "FALSE" || isKeepingDraft === "NO") {
            message.moveToTrash();
        }
        
        var statusCell = sheet.getRange(row + 1, constants.col.status + 1);
        statusCell.setValue("Delivered");
        statusCell.setBackground(constants.statusToColor["Delivered"] || "white");
    }
}

// returns the message or null if it can't be found, shouldn't throw™
function tryGetGmailMessageById(id) { 
    try {
		return GmailApp.getMessageById(id);
	}
	catch(err) {
		// can't be assed to get logging working. err.toString() returns itself (an Exception),
        // err.message.includes and err.includes doesn't work either,
		// there's no documentation and the web debugger isn't good WHINE WHINE WHINE
        //if(!err.toString().includes("Invalid argument: messageId")) {
		//	Logger.log(err);
		//}
		
		return null;
	}
}

function forceImportAllDrafts() {
	PropertiesService.getDocumentProperties().setProperty(constants.propertyKeys.gmailNewestDraftCreationDate, "1");
	initialize();
}